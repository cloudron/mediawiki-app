# MediaWiki Cloudron App

This repository contains the Cloudron app package source for [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.mediawiki.www.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.mediawiki.www.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd mediawiki-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd mediawiki-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Debugging (for developers)

Put this in LocalSettings.php

    $wgDebugLogFile="/tmp/wplog.txt";
    $wgShowExceptionDetails = true

## Authentication Plugin notes

The default MediaWiki installation comes with 3 groups: bot, bureaucrat, sysop. Other groups like `*`, 'user' are
on-the-fly groups (effective group). [https://www.mediawiki.org/wiki/Manual:User_groups_table#Default_MediaWiki_groups](Default Group listing).

Cloudron users' permissions should be managed on User Rights page in mediawiki.

## App upgrade

On the app upgrade administrator password might be changed to something random and it will be shown in the app logs.
If you'd like to change it you should run:

$ php maintenance/run.php changePassword --user=administrator --password=newpassword

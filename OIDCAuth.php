<?php
wfLoadExtension( 'PluggableAuth' );

$wgPluggableAuth_EnableLocalLogin = true;
$wgPluggableAuth_EnableAutoLogin = false;

wfLoadExtension( 'OpenIDConnect' );

$wgPluggableAuth_Config[ "Sign in with " . getenv('CLOUDRON_OIDC_PROVIDER_NAME') ] = [
    'plugin' => 'OpenIDConnect',
    'data' => [
        'providerURL' => getenv('CLOUDRON_OIDC_ISSUER'),
        'clientID' => getenv('CLOUDRON_OIDC_CLIENT_ID'),
        'clientsecret' => getenv('CLOUDRON_OIDC_CLIENT_SECRET'),
        'preferred_username' => 'sub'
    ]
];
$wgOpenIDConnect_MigrateUsersByUserName = true;
$wgOpenIDConnect_MigrateUsersByEmail = true;

$wgGroupPermissions['*']['autocreateaccount'] = true;
# Disallow non-OIDC users to create an account
$wgGroupPermissions['*']['createaccount'] = false;

# https://www.mediawiki.org/wiki/Manual:Preventing_access#Restrict_viewing_of_all_pages
$wgWhitelistRead = array ("Special:Userlogin"); # allow anon to login


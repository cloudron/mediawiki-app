#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /app/data/extensions /app/data/skins /app/data/images /run/mediawiki

install() {
    wiki_title="My Wiki"
    wiki_admin="administrator" # this is a bureaucrat with 'userrights' and 'noratelimit'. "admin" name is reserved!
    wiki_pass="changeme123"

    echo "==> installing mediawiki"

   # this also generates LocalSettings.php in --confpath
    php maintenance/install.php --server "${CLOUDRON_APP_ORIGIN}" --dbname "${CLOUDRON_MYSQL_DATABASE}" --dbserver "${CLOUDRON_MYSQL_HOST}" --dbpass "${CLOUDRON_MYSQL_PASSWORD}" --dbuser "${CLOUDRON_MYSQL_USERNAME}" --pass "${wiki_pass}" --confpath /run/mediawiki "${wiki_title}" "${wiki_admin}"
}

wait_for_mediawiki() {
    while ! curl --fail http://localhost:8000; do
        echo "=> Waiting for app to come up"
        sleep 1
    done
}

reset_admin_password() {
    readonly endpoint=http://localhost:8000/api.php

    token=$(curl -sL -c /tmp/cookies.txt "${endpoint}?action=query&meta=tokens&type=login&format=json" | jq ".query.tokens.logintoken" | sed -e 's/"//g' -e 's,\\,,')

    if ! status=$(curl --fail --data-urlencode "action=clientlogin" --data-urlencode "username=administrator" --data-urlencode "password=changeme123" \
        --data-urlencode "logintoken=${token}" --data-urlencode "loginreturnurl=http://localhost:8000/" --data-urlencode "format=json" \
        -X POST -sL -b /tmp/cookies.txt "${endpoint}" | jq -r ".clientlogin.status"); then
        echo "==> client login fail"
        return 1
    fi

    if [[ "${status}" == "PASS" ]]; then
        echo "==> Resetting default admin password to a random password. See https://docs.cloudron.io/apps/mediawiki/#admin to change it."
        random_password=$(openssl rand -hex 32)
        php maintenance/run.php changePassword --user=administrator --password=${random_password}
    fi
}

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Linking extensions"
for f in $(ls /app/code/extensions.orig); do
    if ! [ -e "/app/data/extensions/$f" ]; then
        ln -s "/app/code/extensions.orig/$f" "/app/data/extensions/$f"
    fi
done

echo "==> Linking skins"
for f in $(ls /app/code/skins.orig); do
    if ! [ -e "/app/data/skins/$f" ]; then
        ln -s "/app/code/skins.orig/$f" "/app/data/skins/$f"
    fi
done

table_count=$(mysql -NB -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" "${CLOUDRON_MYSQL_DATABASE}" 2>/dev/null)

if [[ "${table_count}" == "0" ]]; then
    echo "==> Running install script on first run"
    install
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    [[ -z "${CLOUDRON_OIDC_PROVIDER_NAME:-}" ]] && export CLOUDRON_OIDC_PROVIDER_NAME="Cloudron"
    # terrible hack. we don't use .installed file anymore. the .installed file detects that this is an "old" installation
    # where we have to reset default admin password. new installations have the default admin password
    if [[ -f /app/data/.installed ]]; then
        echo "=> Existing installation detected, will reset admin password"
        (wait_for_mediawiki && reset_admin_password && rm /app/data/.installed) &
    fi
fi

[[ ! -f "/app/data/images/wiki.png" ]] && cp /app/pkg/wiki.png /app/data/images/wiki.png
[[ ! -f "/app/data/LocalSettings.php" ]] && cp /app/pkg/LocalSettings.user.php.template /app/data/LocalSettings.php
cp /app/pkg/LocalSettings.php /run/mediawiki/LocalSettings.php

chown -R www-data.www-data /app/data /run/mediawiki

echo "==> Running maintainence script"
php maintenance/update.php --quick

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND


This app is pre-setup with an admin (bureaucrat) account. The initial credentials are:

**Username**: administrator<br/>
**Password**: changeme123<br/>

<sso>
This wiki is setup to be editable only by Cloudron users. Anonymous users
can read all pages.
</sso>

<nosso>
The wiki is setup to be editable by all registered users and readable by all.
</nosso>


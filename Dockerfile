FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-tags depName=wikimedia/mediawiki versioning=semver
ARG MW_VERSION=1.43.0

# the "x%.*" below removes everything after the last dot i.e it removes the patch version from the version string
RUN curl -L https://releases.wikimedia.org/mediawiki/${MW_VERSION%.*}/mediawiki-${MW_VERSION}.tar.gz | tar zxvf - --strip-components 1

# PluggableAuth extension (https://www.mediawiki.org/wiki/Extension:PluggableAuth)
# https://www.mediawiki.org/wiki/Special:ExtensionDistributor/PluggableAuth
RUN curl -L --fail https://extdist.wmflabs.org/dist/extensions/PluggableAuth-REL1_42-1da98f4.tar.gz | tar -C extensions -xz -f -

# install OpenIDConnect extension (https://www.mediawiki.org/wiki/Special:ExtensionDistributor/OpenIDConnect)
# https://www.mediawiki.org/wiki/Composer#Using_composer-merge-plugin (for composer.local.json)
RUN curl -L --fail     https://extdist.wmflabs.org/dist/extensions/OpenIDConnect-REL1_42-6c28c16.tar.gz | tar -C extensions -xz -f -
COPY composer.local.json /app/code/composer.local.json
RUN composer update

# pear email (https://www.mediawiki.org/wiki/Extension:Email_notification)
RUN pear install mail net_smtp

# move out extensions and skins
RUN mv /app/code/extensions /app/code/extensions.orig && ln -s /app/data/extensions /app/code/extensions && \
    mv /app/code/skins /app/code/skins.orig && ln -s /app/data/skins /app/code/skins

# The LocalSettings.php has to be a broken link during installation time
RUN ln -s /run/mediawiki/LocalSettings.php /app/code/LocalSettings.php && \
    rm -rf /app/code/images && ln -sf /app/data/images /app/code/images

RUN chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/mediawiki.conf /etc/apache2/sites-enabled/mediawiki.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/mediawiki/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

COPY start.sh wiki.png OIDCAuth.php LocalSettings.php LocalSettings.user.php.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]


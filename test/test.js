#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TEST_TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let app, browser, cloudronName;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const email = process.env.EMAIL;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies(); // otherwise username gets prefilled from cookie?
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.linkText('Log in')), TEST_TIMEOUT);
        await browser.findElement(By.linkText('Log in')).click();
        await browser.wait(until.elementLocated(By.id('wpName1')), TEST_TIMEOUT);
        await browser.sleep(6000);
        await browser.findElement(By.id('wpName1')).sendKeys(username);
        await browser.findElement(By.id('wpPassword1')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@value="Log in"]')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/wiki/Main_Page';
        }, TEST_TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.linkText('Log in')), TEST_TIMEOUT);
        await browser.findElement(By.linkText('Log in')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[contains(., "Sign in with")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.linkText('Log out'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.linkText('Log out')), TEST_TIMEOUT);
        await browser.findElement(By.linkText('Log out')).click();
        await browser.wait(until.elementLocated(By.xpath('//strong[text()="You are now logged out."]')), TEST_TIMEOUT);
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//h1/span[contains(text(), "Main Page")]')), TEST_TIMEOUT);
    }


    async function changeUserPermissions(username) {
        await browser.get(`https://${app.fqdn}/wiki/Special:UserRights/${username}`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//th[text()="Groups you can change"]'));
        await browser.findElement(By.id('wpGroup-sysop')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//input[@value="Save user groups"]'));
        await browser.findElement(By.xpath('//input[@value="Save user groups"]')).click();

        await waitForElement(By.xpath('//div[@class="mw-notification-content" and contains(text(), "The user groups") and contains(text(), "have been saved.")]'));
    }

    async function changeProtection() {
        await browser.get('https://' + app.fqdn + '/index.php?title=Main_Page&action=protect');
        // non-sysops will see 'You do not have permission to change protection levels for this page'
        await browser.findElement(By.xpath('//p[contains(text(), "Here you may view and change the protection")]'));
    }

    async function checkUploadedFile() {
        await browser.get('https://' + app.fqdn + '/wiki/File:Logo.png');
        await browser.findElement(By.xpath('//img[@alt="File:Logo.png"]'));
    }

    async function readSourcePage() {
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.linkText('View source')).click();
        await browser.wait(until.elementLocated(By.id('wpTextbox1')), TEST_TIMEOUT);
        const value = await browser.findElement(By.id('wpTextbox1')).getAttribute('value');
        expect(value).to.be('hello world\n');
    }

    async function readEditPage() {
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.linkText('Edit')).click();
        await browser.wait(until.elementLocated(By.id('wpTextbox1')), TEST_TIMEOUT);
        const value = await browser.findElement(By.id('wpTextbox1')).getAttribute('value');
        expect(value).to.be('hello world\n');
    }

    async function edit() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.linkText('Edit')), TEST_TIMEOUT);
        await browser.findElement(By.linkText('Edit')).click();
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.id('wpTextbox1')), TEST_TIMEOUT);
        await browser.findElement(By.id('wpTextbox1')).sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.findElement(By.id('wpTextbox1')).sendKeys('hello world');
        const button = browser.findElement(By.id('wpSave'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.id('wpSave')).click();
        await waitForElement(By.xpath('//span[contains(., "Your edit was saved.")]'));
    }

    async function attachFile() {
        await browser.get('https://' + app.fqdn + '/wiki/Special:Upload');
        await browser.findElement(By.xpath('//input[@type="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        const button = browser.findElement(By.xpath('//input[@value="Upload file" and @type="submit"]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//input[@value="Upload file" and @type="submit"]')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/wiki/File:Logo.png';
        }, TEST_TIMEOUT);
    }

    async function checkNonExistentFile() {
        await browser.get('https://' + app.fqdn + '/wiki/File:Logo.png');
        await browser.findElement(By.xpath('//*[contains(text(), "No file by this name exists")]'));
    }

    async function registerAccount() {
        await browser.get('https://' + app.fqdn + '/index.php?title=Special:CreateAccount');
        await browser.wait(until.elementLocated(By.id('wpName2')), TEST_TIMEOUT);
        await browser.findElement(By.id('wpName2')).sendKeys(username);
        await browser.findElement(By.id('wpPassword2')).sendKeys(password);
        await browser.findElement(By.id('wpRetype')).sendKeys(password);
        await browser.findElement(By.id('wpEmail')).sendKeys(email);
        await browser.findElement(By.xpath('//button[@value="Create your account"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "Your account has been created")]')), TEST_TIMEOUT);
    }

    async function resetAdministratorPassword(password) {
        execSync(`cloudron exec --app ${app.id} -- bash -c 'php maintenance/run.php changePassword --user=administrator --password=${password}'`, { encoding: 'utf8' });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app without sso', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'administrator', 'changeme123'));
    it('can logout', logout);
    it('can register account', registerAccount);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can get the main page', getMainPage);
    it('can admin login', login.bind(null, 'administrator', 'changeme123'));
    it('can login OIDC', loginOIDC.bind(null, username, password, false));
    it('can logout', logout);
    it('can admin login', login.bind(null, 'administrator', 'changeme123'));
    it('can change User permissions for ' + username, changeUserPermissions.bind(null, username));
    it('can logout', logout);
    it('can login OIDC', loginOIDC.bind(null, username, password, true));

    it('can edit', edit);

    it('results in 404 for non-existent file', checkNonExistentFile);
    it('can attach a file', attachFile);

    // https://www.mediawiki.org/wiki/Help:Sysops_and_permissions
    it('can change protection being a sysops', changeProtection);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can read existing page (logged out)', readSourcePage);
    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can read existing page (logged in)', readEditPage);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can read existing page (logged out)', readSourcePage);
    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can read existing page (logged in)', readEditPage);

    it('can get uploaded file', checkUploadedFile);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can read existing page (logged out)', readSourcePage);
    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can read existing page (logged in)', readEditPage);
    it('can get uploaded file', checkUploadedFile);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install previous version', function () { execSync('cloudron install --appstore-id org.mediawiki.www.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can edit', edit);
    it('can attach a file', attachFile);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can read existing page (logged out)', readSourcePage);
    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);

    it('can reset Administrator password', resetAdministratorPassword.bind(null, 'changeme123'));

    it('can admin login', login.bind(null, 'administrator', 'changeme123'));
    it('can change User permissions for ' + username, changeUserPermissions.bind(null, username));
    it('can logout', logout);
    it('can login OIDC', loginOIDC.bind(null, username, password, true));

    it('can read existing page (logged in)', readEditPage);
    it('can get uploaded file', checkUploadedFile);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});

## About

MediaWiki is a free and open-source wiki software package. It serves as the platform
for Wikipedia and the other projects of the Wikimedia Foundation, which deliver
content in over 280 languages to more than half a billion people each month.

## Features

* feature-rich and extensible, both on-wiki and with hundreds of extensions.
* suitable for both small and large sites.
* available in your language.


